package com.lionbridge.content.api.command;

import com.beust.jcommander.Parameter;

public class CreateJobArgs {

    @Parameter(description = "Create-Job")
    public String createJob;

    @Parameter(names = {"-t", "--token"}, description = "Token to access the api", required = true)
    public String token;

}
