package com.lionbridge.content.api.util;

import com.beust.jcommander.JCommander;

import java.util.ResourceBundle;

import static java.util.ResourceBundle.getBundle;

public class JCommanderBuilder {
    private static ResourceBundle messages = getBundle("MessagesBundle");

    public JCommander build() {
        JCommander commander = new JCommander();
        commander.setProgramName(messages.getString("general.program-name"));

        return commander;
    }
}
