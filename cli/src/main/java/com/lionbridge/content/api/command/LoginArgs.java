package com.lionbridge.content.api.command;

import com.beust.jcommander.Parameter;

public class LoginArgs {

    @Parameter(description = "Login")
    public String login;

    @Parameter(names = {"-u", "--user"}, description = "User with access to the api", required = true)
    public String userName;

//    @Parameter(names = {"-p", "--password"}, description = "Password for the user", required = true)
    public String password;

}
