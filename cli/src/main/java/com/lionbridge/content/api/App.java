package com.lionbridge.content.api;

import com.beust.jcommander.JCommander;
import com.lionbridge.content.api.command.CreateJobArgs;
import com.lionbridge.content.api.command.LoginArgs;
import com.lionbridge.content.api.util.JCommanderBuilder;
import io.swagger.client.ApiException;
import io.swagger.client.api.JobApi;
import io.swagger.client.api.TokenApi;
import io.swagger.client.model.CreateJob;
import io.swagger.client.model.CreateToken;
import io.swagger.client.model.Job;
import io.swagger.client.model.Token;

/**
 * Hello world!
 *
 */
public class App 
{
    public static String authorizationToken = "";

    private void processCommandLineArgs(final String[] args) {
        JCommander commander = JCommander.newBuilder()
                                            .programName("Lionbridge Content Api Cli")
                                            .verbose(0)
                                            .build();
        if(args.length==0){
            commander.usage();
        } else {
            String instruction = args[0];
            switch (instruction) {
                case "login":
                    LoginArgs loginArgs = new LoginArgs();
                    commander.addObject(loginArgs);
                    commander.parse(args);
                    loginArgs.password = new String( System.console().readPassword("Password: ") );
                    String result = createToken(loginArgs.userName, loginArgs.password);
                    authorizationToken = result;
                    System.out.println("Generated token: " + result);
                    break;
                case "create-job":
                    CreateJobArgs createJobArgs = new CreateJobArgs();
                    commander.addObject(createJobArgs);
                    commander.parse(args);
                    String createJobResult = createJob(createJobArgs.token);
                    System.out.println("Job has been created");
                    System.out.println(createJobResult);
                    break;
                default:
                    System.out.println(instruction + " not recognized");
            }
        }
    }

    public String createJob(String token) {
        JobApi jobApi = new JobApi();
        CreateJob body = new CreateJob()
                                .jobName("Testing with JCommander")
                                .description("This is just a test")
                                .poReference("3214864")
                                .dueDate("02-02-2019");
        try {
            System.out.println("Requesting job creation");
            System.out.println("Authorization token is: " + token);
            Job job = jobApi.jobsPost(token, body);
            return job.toString();
        } catch (ApiException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public String createToken( String username, String password) {
        Token result = null;
        try {
            TokenApi apiInstance = new TokenApi();
            CreateToken body = new CreateToken();
            body.setUsername(username);
            body.setPassword(password);
            result = apiInstance.oauth2TokenPost(body);
            return result.getAccessToken();
        } catch (ApiException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public static void main( String[] args )
    {
        new App().processCommandLineArgs(args);
    }
}
