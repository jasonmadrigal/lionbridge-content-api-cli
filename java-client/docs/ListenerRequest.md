
# ListenerRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**listenerId** | **String** | The Id of the listener that generated this request. | 
**jobId** | **String** | The Id of the translation job for this update. | 
**requestIds** | **List&lt;String&gt;** | The request Ids for this update. |  [optional]
**statusCode** | [**StatusCodeEnum**](StatusCodeEnum.md) |  |  [optional]



