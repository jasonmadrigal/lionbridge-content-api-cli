# ProviderApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**providersGet**](ProviderApi.md#providersGet) | **GET** /providers | Get all configured translation providers.
[**providersProviderIdGet**](ProviderApi.md#providersProviderIdGet) | **GET** /providers/{providerId} | Get a translation provider.


<a name="providersGet"></a>
# **providersGet**
> List&lt;Provider&gt; providersGet(authorization)

Get all configured translation providers.

Get all configured translation providers.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProviderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

ProviderApi apiInstance = new ProviderApi();
String authorization = "authorization_example"; // String | Oauth2 token
try {
    List<Provider> result = apiInstance.providersGet(authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProviderApi#providersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |

### Return type

[**List&lt;Provider&gt;**](Provider.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="providersProviderIdGet"></a>
# **providersProviderIdGet**
> Provider providersProviderIdGet(authorization, providerId)

Get a translation provider.

Get a translation provider.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProviderApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

ProviderApi apiInstance = new ProviderApi();
String authorization = "authorization_example"; // String | Oauth2 token
String providerId = "providerId_example"; // String | The ID of the translation provider.
try {
    Provider result = apiInstance.providersProviderIdGet(authorization, providerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProviderApi#providersProviderIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **providerId** | **String**| The ID of the translation provider. |

### Return type

[**Provider**](Provider.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

