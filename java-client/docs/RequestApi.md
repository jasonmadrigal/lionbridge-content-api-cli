# RequestApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsJobIdRequestsAddPost**](RequestApi.md#jobsJobIdRequestsAddPost) | **POST** /jobs/{jobId}/requests/add | Create new translation requests in a job.
[**jobsJobIdRequestsAddfilePost**](RequestApi.md#jobsJobIdRequestsAddfilePost) | **POST** /jobs/{jobId}/requests/addfile | Add translation requests to a job based on a file.
[**jobsJobIdRequestsApprovePut**](RequestApi.md#jobsJobIdRequestsApprovePut) | **PUT** /jobs/{jobId}/requests/approve | Approve translated content of specified request(s).
[**jobsJobIdRequestsGet**](RequestApi.md#jobsJobIdRequestsGet) | **GET** /jobs/{jobId}/requests | Get all translation requests in a job.
[**jobsJobIdRequestsPut**](RequestApi.md#jobsJobIdRequestsPut) | **PUT** /jobs/{jobId}/requests | Get specified translation requests in a job.
[**jobsJobIdRequestsRejectPut**](RequestApi.md#jobsJobIdRequestsRejectPut) | **PUT** /jobs/{jobId}/requests/reject | Reject translated content of specified request(s).
[**jobsJobIdRequestsRequestIdDelete**](RequestApi.md#jobsJobIdRequestsRequestIdDelete) | **DELETE** /jobs/{jobId}/requests/{requestId} | Delete the translation request in a job identified by requestId
[**jobsJobIdRequestsRequestIdGet**](RequestApi.md#jobsJobIdRequestsRequestIdGet) | **GET** /jobs/{jobId}/requests/{requestId} | Get the translation request in a job identified by requestId
[**jobsJobIdRequestsUpdatecontentPut**](RequestApi.md#jobsJobIdRequestsUpdatecontentPut) | **PUT** /jobs/{jobId}/requests/updatecontent | Add content to existing translation requests.
[**jobsJobIdRequestsUpdatefilecontentPut**](RequestApi.md#jobsJobIdRequestsUpdatefilecontentPut) | **PUT** /jobs/{jobId}/requests/updatefilecontent | Add a file to existing translation requests.


<a name="jobsJobIdRequestsAddPost"></a>
# **jobsJobIdRequestsAddPost**
> List&lt;Request&gt; jobsJobIdRequestsAddPost(authorization, jobId, body)

Create new translation requests in a job.

Create new translation requests in a job.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
CreateRequestKeyValue body = new CreateRequestKeyValue(); // CreateRequestKeyValue | Created Request object
try {
    List<Request> result = apiInstance.jobsJobIdRequestsAddPost(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsAddPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |
 **body** | [**CreateRequestKeyValue**](CreateRequestKeyValue.md)| Created Request object |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsAddfilePost"></a>
# **jobsJobIdRequestsAddfilePost**
> List&lt;Request&gt; jobsJobIdRequestsAddfilePost(authorization, jobId, body)

Add translation requests to a job based on a file.

Adds translation requests to a job, using a previously uploaded file as the source content.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | Job ID.
CreateRequestFile body = new CreateRequestFile(); // CreateRequestFile | Created Request object
try {
    List<Request> result = apiInstance.jobsJobIdRequestsAddfilePost(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsAddfilePost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| Job ID. |
 **body** | [**CreateRequestFile**](CreateRequestFile.md)| Created Request object |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsApprovePut"></a>
# **jobsJobIdRequestsApprovePut**
> List&lt;Request&gt; jobsJobIdRequestsApprovePut(authorization, jobId, body)

Approve translated content of specified request(s).

Approve translated content of specified request(s).

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
ArrayOfRequestIds body = new ArrayOfRequestIds(); // ArrayOfRequestIds | Array of RequestIds
try {
    List<Request> result = apiInstance.jobsJobIdRequestsApprovePut(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsApprovePut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |
 **body** | [**ArrayOfRequestIds**](ArrayOfRequestIds.md)| Array of RequestIds |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsGet"></a>
# **jobsJobIdRequestsGet**
> List&lt;Request&gt; jobsJobIdRequestsGet(authorization, jobId)

Get all translation requests in a job.

Get all translation requests in a job.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
try {
    List<Request> result = apiInstance.jobsJobIdRequestsGet(authorization, jobId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsPut"></a>
# **jobsJobIdRequestsPut**
> List&lt;Request&gt; jobsJobIdRequestsPut(authorization, jobId, body)

Get specified translation requests in a job.

Get specified translation requests in a job.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
ArrayOfRequestIds body = new ArrayOfRequestIds(); // ArrayOfRequestIds | Array of RequestIds
try {
    List<Request> result = apiInstance.jobsJobIdRequestsPut(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |
 **body** | [**ArrayOfRequestIds**](ArrayOfRequestIds.md)| Array of RequestIds |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsRejectPut"></a>
# **jobsJobIdRequestsRejectPut**
> List&lt;Request&gt; jobsJobIdRequestsRejectPut(authorization, jobId, body)

Reject translated content of specified request(s).

Reject translated content of specified request(s).

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
ArrayOfRequestIdsNote body = new ArrayOfRequestIdsNote(); // ArrayOfRequestIdsNote | Array of RequestIds
try {
    List<Request> result = apiInstance.jobsJobIdRequestsRejectPut(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsRejectPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |
 **body** | [**ArrayOfRequestIdsNote**](ArrayOfRequestIdsNote.md)| Array of RequestIds |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsRequestIdDelete"></a>
# **jobsJobIdRequestsRequestIdDelete**
> Request jobsJobIdRequestsRequestIdDelete(authorization, jobId, requestId)

Delete the translation request in a job identified by requestId

Delete the translation request in a job identified by requestId

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job.
String requestId = "requestId_example"; // String | The ID of the translation request.
try {
    Request result = apiInstance.jobsJobIdRequestsRequestIdDelete(authorization, jobId, requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsRequestIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job. |
 **requestId** | **String**| The ID of the translation request. |

### Return type

[**Request**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsRequestIdGet"></a>
# **jobsJobIdRequestsRequestIdGet**
> Request jobsJobIdRequestsRequestIdGet(authorization, jobId, requestId)

Get the translation request in a job identified by requestId

Get the translation request in a job identified by requestId.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job.
String requestId = "requestId_example"; // String | The ID of the translation request.
try {
    Request result = apiInstance.jobsJobIdRequestsRequestIdGet(authorization, jobId, requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsRequestIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job. |
 **requestId** | **String**| The ID of the translation request. |

### Return type

[**Request**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsUpdatecontentPut"></a>
# **jobsJobIdRequestsUpdatecontentPut**
> List&lt;Request&gt; jobsJobIdRequestsUpdatecontentPut(authorization, jobId, body)

Add content to existing translation requests.

Add key-value pairs to existing translation requests.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
UpdateRequestsKeyValue body = new UpdateRequestsKeyValue(); // UpdateRequestsKeyValue | Updated Request object
try {
    List<Request> result = apiInstance.jobsJobIdRequestsUpdatecontentPut(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsUpdatecontentPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |
 **body** | [**UpdateRequestsKeyValue**](UpdateRequestsKeyValue.md)| Updated Request object |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdRequestsUpdatefilecontentPut"></a>
# **jobsJobIdRequestsUpdatefilecontentPut**
> List&lt;Request&gt; jobsJobIdRequestsUpdatefilecontentPut(authorization, jobId, body)

Add a file to existing translation requests.

Associates a file as the source content to existing translation requests.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RequestApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

RequestApi apiInstance = new RequestApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | Job ID.
UpdateRequestsFile body = new UpdateRequestsFile(); // UpdateRequestsFile | Updated Request object
try {
    List<Request> result = apiInstance.jobsJobIdRequestsUpdatefilecontentPut(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RequestApi#jobsJobIdRequestsUpdatefilecontentPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| Job ID. |
 **body** | [**UpdateRequestsFile**](UpdateRequestsFile.md)| Updated Request object |

### Return type

[**List&lt;Request&gt;**](Request.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

