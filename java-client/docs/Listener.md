
# Listener

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**listenerId** | **String** | The Id of the Listener. | 
**jobId** | **String** | The Id of the translation job. |  [optional]
**uri** | **String** | The URI that Clay Tablet should ping once a specific event has fired. | 
**type** | [**ListenerTypeEnum**](ListenerTypeEnum.md) |  | 
**statusCodes** | [**List&lt;StatusCodeEnum&gt;**](StatusCodeEnum.md) | Status code types that you wish to monitor. If none specified, then all will be monitored. |  [optional]
**authType** | [**ListenerAuthEnum**](ListenerAuthEnum.md) | Authentication type for listener, if left empty then CTT_GENERATED_TOKEN will be used. |  [optional]



