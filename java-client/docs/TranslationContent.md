
# TranslationContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **String** | ID of the translation request. |  [optional]
**sourceNativeId** | **String** | Source ID of the request in the content system. |  [optional]
**sourceNativeLanguageCode** | **String** | Source language code of the request in the content system. |  [optional]
**targetNativeId** | **String** | Target ID of the request in the content system. |  [optional]
**targetNativeLanguageCode** | **String** | Target language code of the request in the content system. |  [optional]
**structuredContent** | [**List&lt;KeyValuePair&gt;**](KeyValuePair.md) |  |  [optional]
**unstructuredContent** | **List&lt;String&gt;** |  |  [optional]



