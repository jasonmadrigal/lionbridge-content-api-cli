# TokenApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**oauth2TokenGet**](TokenApi.md#oauth2TokenGet) | **GET** /oauth2/token | Get a new token from Zendesk authentication.
[**oauth2TokenPost**](TokenApi.md#oauth2TokenPost) | **POST** /oauth2/token | Get a new token.


<a name="oauth2TokenGet"></a>
# **oauth2TokenGet**
> Token oauth2TokenGet(code)

Get a new token from Zendesk authentication.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.TokenApi;


TokenApi apiInstance = new TokenApi();
String code = "code_example"; // String | The code provided by Zendesk oauth.
try {
    Token result = apiInstance.oauth2TokenGet(code);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TokenApi#oauth2TokenGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**| The code provided by Zendesk oauth. |

### Return type

[**Token**](Token.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="oauth2TokenPost"></a>
# **oauth2TokenPost**
> Token oauth2TokenPost(body)

Get a new token.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.TokenApi;


TokenApi apiInstance = new TokenApi();
CreateToken body = new CreateToken(); // CreateToken | Created token object
try {
    Token result = apiInstance.oauth2TokenPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TokenApi#oauth2TokenPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateToken**](CreateToken.md)| Created token object |

### Return type

[**Token**](Token.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

