
# ProviderId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**providerId** | **String** | Unique identifier representing a provider. |  [optional]



