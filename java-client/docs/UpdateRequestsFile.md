
# UpdateRequestsFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestIds** | **List&lt;String&gt;** |  |  [optional]
**fileId** | **String** | ID of source file to use in this request. | 



