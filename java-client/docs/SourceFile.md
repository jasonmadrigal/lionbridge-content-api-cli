
# SourceFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileId** | **String** | Unique identifier representing source file. | 
**jobId** | **String** | Unique identifier representing a translation job. | 
**filename** | **String** | Name of file | 
**filetype** | **String** | Type of file. | 



