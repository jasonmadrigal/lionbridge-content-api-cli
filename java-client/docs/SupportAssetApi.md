# SupportAssetApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsJobIdSupportassetsGet**](SupportAssetApi.md#jobsJobIdSupportassetsGet) | **GET** /jobs/{jobId}/supportassets | Get all support assets in a job.
[**jobsJobIdSupportassetsPost**](SupportAssetApi.md#jobsJobIdSupportassetsPost) | **POST** /jobs/{jobId}/supportassets | Add support asset to a job.
[**jobsJobIdSupportassetsSupportassetIdDelete**](SupportAssetApi.md#jobsJobIdSupportassetsSupportassetIdDelete) | **DELETE** /jobs/{jobId}/supportassets/{supportassetId} | Delete the support asset in a job identified by supportassetId
[**jobsJobIdSupportassetsSupportassetIdGet**](SupportAssetApi.md#jobsJobIdSupportassetsSupportassetIdGet) | **GET** /jobs/{jobId}/supportassets/{supportassetId} | Get the support asset in a job identified by supportassetId


<a name="jobsJobIdSupportassetsGet"></a>
# **jobsJobIdSupportassetsGet**
> List&lt;SupportAsset&gt; jobsJobIdSupportassetsGet(authorization, jobId)

Get all support assets in a job.

Get all support assets in a job.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupportAssetApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

SupportAssetApi apiInstance = new SupportAssetApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
try {
    List<SupportAsset> result = apiInstance.jobsJobIdSupportassetsGet(authorization, jobId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupportAssetApi#jobsJobIdSupportassetsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |

### Return type

[**List&lt;SupportAsset&gt;**](SupportAsset.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdSupportassetsPost"></a>
# **jobsJobIdSupportassetsPost**
> SupportAsset jobsJobIdSupportassetsPost(authorization, jobId, body)

Add support asset to a job.

Add a support asset to a job based on a file    

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupportAssetApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

SupportAssetApi apiInstance = new SupportAssetApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
CreateSupportAsset body = new CreateSupportAsset(); // CreateSupportAsset | Created Support Asset object
try {
    SupportAsset result = apiInstance.jobsJobIdSupportassetsPost(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupportAssetApi#jobsJobIdSupportassetsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |
 **body** | [**CreateSupportAsset**](CreateSupportAsset.md)| Created Support Asset object |

### Return type

[**SupportAsset**](SupportAsset.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdSupportassetsSupportassetIdDelete"></a>
# **jobsJobIdSupportassetsSupportassetIdDelete**
> jobsJobIdSupportassetsSupportassetIdDelete(authorization, jobId, supportassetId)

Delete the support asset in a job identified by supportassetId

Delete the support asset in a job identified by supportassetId

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupportAssetApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

SupportAssetApi apiInstance = new SupportAssetApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job.
String supportassetId = "supportassetId_example"; // String | The ID of the support asset.
try {
    apiInstance.jobsJobIdSupportassetsSupportassetIdDelete(authorization, jobId, supportassetId);
} catch (ApiException e) {
    System.err.println("Exception when calling SupportAssetApi#jobsJobIdSupportassetsSupportassetIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job. |
 **supportassetId** | **String**| The ID of the support asset. |

### Return type

null (empty response body)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdSupportassetsSupportassetIdGet"></a>
# **jobsJobIdSupportassetsSupportassetIdGet**
> SupportAsset jobsJobIdSupportassetsSupportassetIdGet(authorization, jobId, supportassetId)

Get the support asset in a job identified by supportassetId

Get the support asset in a job identified by supportassetId.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SupportAssetApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

SupportAssetApi apiInstance = new SupportAssetApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job.
String supportassetId = "supportassetId_example"; // String | The ID of the support asset.
try {
    SupportAsset result = apiInstance.jobsJobIdSupportassetsSupportassetIdGet(authorization, jobId, supportassetId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SupportAssetApi#jobsJobIdSupportassetsSupportassetIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job. |
 **supportassetId** | **String**| The ID of the support asset. |

### Return type

[**SupportAsset**](SupportAsset.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

