
# Request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **String** | Unique identifier representing the translation request. | 
**jobId** | **String** | Unique identifier representing a translation job. | 
**requestName** | **String** | Name of the translation request. | 
**statusCode** | [**StatusCode**](StatusCode.md) |  |  [optional]
**hasError** | **Boolean** | If the translation request currently has an error. |  [optional]
**latestErrorMessage** | **String** | The message for the most recent error affecting a translation request. |  [optional]
**sourceNativeId** | **String** | Source ID of the request in the content system. | 
**sourceNativeLanguageCode** | **String** | Source language code of the request in the content system. | 
**targetNativeId** | **String** | Target ID of the request in the content system. |  [optional]
**targetNativeLanguageCode** | **String** | Target language code of the request in the content system. | 
**createdDate** | [**OffsetDateTime**](OffsetDateTime.md) | Date-time signature of when the translation request was created. |  [optional]
**modifiedDate** | [**OffsetDateTime**](OffsetDateTime.md) | Date-time signature of when the translation request was last modified. |  [optional]
**wordCount** | **Integer** | Word count in translation request. |  [optional]
**fileId** | **String** | ID of source file if file exists. |  [optional]
**fileType** | **String** | MIME type of the source file of the request. This should be left empty for name-value-pair requests. |  [optional]



