
# JobStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalRequests** | **Integer** | Total number of requests in a job. |  [optional]
**totalSentout** | **Integer** | Total number of requests sent out as part of a job. |  [optional]
**totalInTranslation** | **Integer** | Total number of requests received by translation provider. |  [optional]
**totalError** | **Integer** | Total number of requests that have an error. |  [optional]
**totalReceived** | **Integer** | Total number of requests returned from translation provider. |  [optional]
**totalCompleted** | **Integer** | Total number of completed requests in a job. |  [optional]



