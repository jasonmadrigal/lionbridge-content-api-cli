# SourceFileApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsJobIdUploadPost**](SourceFileApi.md#jobsJobIdUploadPost) | **POST** /jobs/{jobId}/upload | Upload a binary file to a job.


<a name="jobsJobIdUploadPost"></a>
# **jobsJobIdUploadPost**
> SourceFile jobsJobIdUploadPost(authorization, jobId, fileName, fileType, sourceFile)

Upload a binary file to a job.

Adds a file to the specified jobId. This can then be used to create a translation request. Or it can be used as a support asset to a job that has not been submitted.  *Note:* The legacy endpoint is still supported: https://ctt-rest-api-servlet-us-east-1.clay-tablet.net 

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.SourceFileApi;


SourceFileApi apiInstance = new SourceFileApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | Job ID.
String fileName = "fileName_example"; // String | Name of file.
String fileType = "fileType_example"; // String | MIME type of the source file of the request. If empty, a default will be supplied based on the extension extracted from the name of the request, or 'application/octet-stream' if no extension can be found from the name of the request.
File sourceFile = new File("/path/to/file.txt"); // File | The file of the source content for the request(s).
try {
    SourceFile result = apiInstance.jobsJobIdUploadPost(authorization, jobId, fileName, fileType, sourceFile);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SourceFileApi#jobsJobIdUploadPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| Job ID. |
 **fileName** | **String**| Name of file. |
 **fileType** | **String**| MIME type of the source file of the request. If empty, a default will be supplied based on the extension extracted from the name of the request, or &#39;application/octet-stream&#39; if no extension can be found from the name of the request. |
 **sourceFile** | **File**| The file of the source content for the request(s). |

### Return type

[**SourceFile**](SourceFile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

