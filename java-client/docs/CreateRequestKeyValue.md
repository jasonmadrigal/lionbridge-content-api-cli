
# CreateRequestKeyValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestName** | **String** | Name of the translation request. | 
**sourceNativeId** | **String** | Source ID of the request in the content system. | 
**sourceNativeLanguageCode** | **String** | Source language code of the request in the content system. | 
**targetNativeIds** | **List&lt;String&gt;** |  |  [optional]
**targetNativeLanguageCodes** | **List&lt;String&gt;** |  | 
**wordCount** | **Integer** | Word count in translation request. |  [optional]
**fieldNames** | **List&lt;String&gt;** |  |  [optional]
**fieldValues** | **List&lt;String&gt;** |  |  [optional]



