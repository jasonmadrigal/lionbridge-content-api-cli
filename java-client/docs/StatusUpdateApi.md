# StatusUpdateApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**statusupdatesGet**](StatusUpdateApi.md#statusupdatesGet) | **GET** /statusupdates | Get all unacknowledged status updates.
[**statusupdatesListenersGet**](StatusUpdateApi.md#statusupdatesListenersGet) | **GET** /statusupdates/listeners | Query all Listeners.
[**statusupdatesListenersListenerIdDelete**](StatusUpdateApi.md#statusupdatesListenersListenerIdDelete) | **DELETE** /statusupdates/listeners/{listenerId} | Delete a Listener.
[**statusupdatesListenersListenerIdGet**](StatusUpdateApi.md#statusupdatesListenersListenerIdGet) | **GET** /statusupdates/listeners/{listenerId} | Query listener information.
[**statusupdatesListenersPost**](StatusUpdateApi.md#statusupdatesListenersPost) | **POST** /statusupdates/listeners | Create a new Listener.
[**statusupdatesUpdateIdAcknowledgePut**](StatusUpdateApi.md#statusupdatesUpdateIdAcknowledgePut) | **PUT** /statusupdates/{updateId}/acknowledge | Acknowledge a status update.


<a name="statusupdatesGet"></a>
# **statusupdatesGet**
> List&lt;StatusUpdate&gt; statusupdatesGet(authorization)

Get all unacknowledged status updates.

Get all unacknowledged status updates.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatusUpdateApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

StatusUpdateApi apiInstance = new StatusUpdateApi();
String authorization = "authorization_example"; // String | Oauth2 token
try {
    List<StatusUpdate> result = apiInstance.statusupdatesGet(authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusUpdateApi#statusupdatesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |

### Return type

[**List&lt;StatusUpdate&gt;**](StatusUpdate.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="statusupdatesListenersGet"></a>
# **statusupdatesListenersGet**
> List&lt;Listener&gt; statusupdatesListenersGet(authorization)

Query all Listeners.

The Listeners endpoint returns information about the Clay Tablet Listeners. The response includes listener IDs and detailed information of all listeners for the given user.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatusUpdateApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

StatusUpdateApi apiInstance = new StatusUpdateApi();
String authorization = "authorization_example"; // String | Oauth2 token
try {
    List<Listener> result = apiInstance.statusupdatesListenersGet(authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusUpdateApi#statusupdatesListenersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |

### Return type

[**List&lt;Listener&gt;**](Listener.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="statusupdatesListenersListenerIdDelete"></a>
# **statusupdatesListenersListenerIdDelete**
> Listener statusupdatesListenersListenerIdDelete(authorization, listenerId)

Delete a Listener.

Delete a Listener.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatusUpdateApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

StatusUpdateApi apiInstance = new StatusUpdateApi();
String authorization = "authorization_example"; // String | Oauth2 token
String listenerId = "listenerId_example"; // String | The ID of the specified Listener.
try {
    Listener result = apiInstance.statusupdatesListenersListenerIdDelete(authorization, listenerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusUpdateApi#statusupdatesListenersListenerIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **listenerId** | **String**| The ID of the specified Listener. |

### Return type

[**Listener**](Listener.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="statusupdatesListenersListenerIdGet"></a>
# **statusupdatesListenersListenerIdGet**
> Listener statusupdatesListenersListenerIdGet(authorization, listenerId)

Query listener information.

Returns the listener specified by listenerId.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatusUpdateApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

StatusUpdateApi apiInstance = new StatusUpdateApi();
String authorization = "authorization_example"; // String | Oauth2 token
String listenerId = "listenerId_example"; // String | The ID of the Listener being queried.
try {
    Listener result = apiInstance.statusupdatesListenersListenerIdGet(authorization, listenerId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusUpdateApi#statusupdatesListenersListenerIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **listenerId** | **String**| The ID of the Listener being queried. |

### Return type

[**Listener**](Listener.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="statusupdatesListenersPost"></a>
# **statusupdatesListenersPost**
> Listener statusupdatesListenersPost(authorization, body)

Create a new Listener.

You define: * a listener for jobs or requests * the status(es) for receiving the requests * a URI for receiving HTTP requests from Clay Tablet  When a job/request reaches a status for which you have a listener, Clay Tablet sends an HTTP request. Clay Tablet sends a request for every matching listener.  The HTTP requests that Clay Tablet sends to the user-defined URI look like:  1. Authorization header   * CTT_GENERATED_TOKEN     * Generated like this: http://www.jokecamp.com/blog/examples-of-creating-base64-hashes-using-hmac-sha256-in-different-languages/#java     * Secret &#x3D; the token used to create the listener     * Message &#x3D; json message body   * NONE     * No authorization header will be included in request 2. HTTP Request that Clay Tablet sends   * PUT request   * Expects to return 200 status code on success   * Body format is similar to:     * &#x60;&#x60;&#x60;     {      \&quot;listenerId\&quot;: \&quot;my-listener-id\&quot;,      \&quot;jobId\&quot;: \&quot;my-job-id\&quot;,      \&quot;requestIds\&quot;: [\&quot;my-request-id-1\&quot;, \&quot;my-request-id-2\&quot;],      \&quot;statusCode\&quot;: \&quot;REVIEW_TRANSLATION\&quot;    }    &#x60;&#x60;&#x60;   * *requestIds* is optional. For example, Clay Tablet does not send it when the listener type is JOB_STATUS_UPDATED.  If the webhook does not return a 200 code, then Clay Tablet assumes that the request failed. Clay Tablet resends a failed request up to 20 times over 13 hours: after that it does not resend the request. 

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatusUpdateApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

StatusUpdateApi apiInstance = new StatusUpdateApi();
String authorization = "authorization_example"; // String | Oauth2 token
CreateListener body = new CreateListener(); // CreateListener | Created Listener object
try {
    Listener result = apiInstance.statusupdatesListenersPost(authorization, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusUpdateApi#statusupdatesListenersPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **body** | [**CreateListener**](CreateListener.md)| Created Listener object |

### Return type

[**Listener**](Listener.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="statusupdatesUpdateIdAcknowledgePut"></a>
# **statusupdatesUpdateIdAcknowledgePut**
> StatusUpdate statusupdatesUpdateIdAcknowledgePut(authorization, updateId)

Acknowledge a status update.

Acknowledges a status update, which results in specified ID no longer appearing in future /statusupdates request.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatusUpdateApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

StatusUpdateApi apiInstance = new StatusUpdateApi();
String authorization = "authorization_example"; // String | Oauth2 token
String updateId = "updateId_example"; // String | The ID of the status update being acknowledged.
try {
    StatusUpdate result = apiInstance.statusupdatesUpdateIdAcknowledgePut(authorization, updateId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatusUpdateApi#statusupdatesUpdateIdAcknowledgePut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **updateId** | **String**| The ID of the status update being acknowledged. |

### Return type

[**StatusUpdate**](StatusUpdate.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

