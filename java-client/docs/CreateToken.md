
# CreateToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | The username. | 
**password** | **String** | The password for associated username. | 



