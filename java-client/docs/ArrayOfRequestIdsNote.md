
# ArrayOfRequestIdsNote

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestIds** | **List&lt;String&gt;** |  | 
**note** | **String** | Note which explains the reason(s) for rejection. |  [optional]



