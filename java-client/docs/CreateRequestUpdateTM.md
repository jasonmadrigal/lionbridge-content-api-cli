
# CreateRequestUpdateTM

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileId** | **String** | ID of file containing updated content to use in this UpdateTM. |  [optional]
**sourceNativeLanguageCode** | **String** | Source language code of the update in the content system. |  [optional]
**targetNativeLanguageCode** | **String** | Target language code of the update in the content system. |  [optional]



