
# Provider

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**providerId** | **String** | ID of the provider. |  [optional]
**providerName** | **String** | Name of the provider. |  [optional]



