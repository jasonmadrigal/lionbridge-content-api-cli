# TranslationContentApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsJobIdRequestsRequestIdRetrieveGet**](TranslationContentApi.md#jobsJobIdRequestsRequestIdRetrieveGet) | **GET** /jobs/{jobId}/requests/{requestId}/retrieve | Retrieve the target content for translation request(s).


<a name="jobsJobIdRequestsRequestIdRetrieveGet"></a>
# **jobsJobIdRequestsRequestIdRetrieveGet**
> List&lt;TranslationContent&gt; jobsJobIdRequestsRequestIdRetrieveGet(authorization, jobId, requestId)

Retrieve the target content for translation request(s).

Retrieve the target content for translation request(s).

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TranslationContentApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

TranslationContentApi apiInstance = new TranslationContentApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job.
String requestId = "requestId_example"; // String | The ID of the translation request.
try {
    List<TranslationContent> result = apiInstance.jobsJobIdRequestsRequestIdRetrieveGet(authorization, jobId, requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TranslationContentApi#jobsJobIdRequestsRequestIdRetrieveGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job. |
 **requestId** | **String**| The ID of the translation request. |

### Return type

[**List&lt;TranslationContent&gt;**](TranslationContent.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

