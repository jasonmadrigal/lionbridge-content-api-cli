
# UpdateRequestsKeyValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestIds** | **List&lt;String&gt;** |  |  [optional]
**fieldNames** | **List&lt;String&gt;** |  | 
**fieldValues** | **List&lt;String&gt;** |  | 



