# FileApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsJobIdRequestsRequestIdRetrievefileGet**](FileApi.md#jobsJobIdRequestsRequestIdRetrievefileGet) | **GET** /jobs/{jobId}/requests/{requestId}/retrievefile | Retrieve the target content for translation request as a file.


<a name="jobsJobIdRequestsRequestIdRetrievefileGet"></a>
# **jobsJobIdRequestsRequestIdRetrievefileGet**
> File jobsJobIdRequestsRequestIdRetrievefileGet(authorization, jobId, requestId)

Retrieve the target content for translation request as a file.

Retrieve the target content for translation request as a file. *Note:* The legacy endpoint is still supported: https://ctt-rest-api-servlet-us-east-1.clay-tablet.net 

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FileApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

FileApi apiInstance = new FileApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job.
String requestId = "requestId_example"; // String | The ID of the translation request.
try {
    File result = apiInstance.jobsJobIdRequestsRequestIdRetrievefileGet(authorization, jobId, requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#jobsJobIdRequestsRequestIdRetrievefileGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job. |
 **requestId** | **String**| The ID of the translation request. |

### Return type

[**File**](File.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/octet-stream

