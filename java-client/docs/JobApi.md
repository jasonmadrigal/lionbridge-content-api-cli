# JobApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsGet**](JobApi.md#jobsGet) | **GET** /jobs | Query all jobs.
[**jobsJobIdArchivePut**](JobApi.md#jobsJobIdArchivePut) | **PUT** /jobs/{jobId}/archive | Archive a job.
[**jobsJobIdDelete**](JobApi.md#jobsJobIdDelete) | **DELETE** /jobs/{jobId} | Delete a job.
[**jobsJobIdGet**](JobApi.md#jobsJobIdGet) | **GET** /jobs/{jobId} | Query job information.
[**jobsJobIdPut**](JobApi.md#jobsJobIdPut) | **PUT** /jobs/{jobId} | Update a job&#39;s information.
[**jobsJobIdSubmitPut**](JobApi.md#jobsJobIdSubmitPut) | **PUT** /jobs/{jobId}/submit | Send/Submit job to provider.
[**jobsJobIdUnarchivePut**](JobApi.md#jobsJobIdUnarchivePut) | **PUT** /jobs/{jobId}/unarchive | Unarchive a job.
[**jobsPost**](JobApi.md#jobsPost) | **POST** /jobs | Create a new job.


<a name="jobsGet"></a>
# **jobsGet**
> List&lt;Job&gt; jobsGet(authorization, fetchType, includeArchived)

Query all jobs.

The jobs endpoint returns information about the Clay Tablet translation jobs. The response includes job IDs and detailed information of all jobs for the given user.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
String fetchType = "full"; // String | Specifies what level of detail is returned for a job.
Boolean includeArchived = false; // Boolean | Include archived jobs in job listing.
try {
    List<Job> result = apiInstance.jobsGet(authorization, fetchType, includeArchived);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **fetchType** | **String**| Specifies what level of detail is returned for a job. | [optional] [default to full] [enum: full, fullWithStats]
 **includeArchived** | **Boolean**| Include archived jobs in job listing. | [optional] [default to false]

### Return type

[**List&lt;Job&gt;**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdArchivePut"></a>
# **jobsJobIdArchivePut**
> Job jobsJobIdArchivePut(authorization, jobId)

Archive a job.

Archive a translation job. Archived jobs are not returned when users query for a list of active jobs.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
try {
    Job result = apiInstance.jobsJobIdArchivePut(authorization, jobId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsJobIdArchivePut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |

### Return type

[**Job**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdDelete"></a>
# **jobsJobIdDelete**
> Job jobsJobIdDelete(authorization, jobId)

Delete a job.

Delete a translation job.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
try {
    Job result = apiInstance.jobsJobIdDelete(authorization, jobId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsJobIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |

### Return type

[**Job**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdGet"></a>
# **jobsJobIdGet**
> Job jobsJobIdGet(authorization, jobId, fetchType)

Query job information.

Returns job data.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job being queried.
String fetchType = "full"; // String | Specifies what level of detail is returned for a job.
try {
    Job result = apiInstance.jobsJobIdGet(authorization, jobId, fetchType);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsJobIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job being queried. |
 **fetchType** | **String**| Specifies what level of detail is returned for a job. | [optional] [default to full] [enum: full, fullWithStats]

### Return type

[**Job**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdPut"></a>
# **jobsJobIdPut**
> Job jobsJobIdPut(authorization, jobId, job)

Update a job&#39;s information.

Update a job&#39;s information. Only jobs that have not been submitted can be updated.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the job being updated.
UpdateJob job = new UpdateJob(); // UpdateJob | The job being updated.
try {
    Job result = apiInstance.jobsJobIdPut(authorization, jobId, job);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsJobIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the job being updated. |
 **job** | [**UpdateJob**](UpdateJob.md)| The job being updated. |

### Return type

[**Job**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdSubmitPut"></a>
# **jobsJobIdSubmitPut**
> Job jobsJobIdSubmitPut(authorization, jobId, body)

Send/Submit job to provider.

Submit job to provider, when all translation requests have been added.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
ProviderId body = new ProviderId(); // ProviderId | The ID of the provider the job will be sent to. This information can be omitted if the site has only one translation provider.
try {
    Job result = apiInstance.jobsJobIdSubmitPut(authorization, jobId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsJobIdSubmitPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |
 **body** | [**ProviderId**](ProviderId.md)| The ID of the provider the job will be sent to. This information can be omitted if the site has only one translation provider. |

### Return type

[**Job**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsJobIdUnarchivePut"></a>
# **jobsJobIdUnarchivePut**
> Job jobsJobIdUnarchivePut(authorization, jobId)

Unarchive a job.

Unarchive a translation job. Unarchived jobs are returned when users query for a list of active jobs.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | The ID of the specified job.
try {
    Job result = apiInstance.jobsJobIdUnarchivePut(authorization, jobId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsJobIdUnarchivePut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| The ID of the specified job. |

### Return type

[**Job**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="jobsPost"></a>
# **jobsPost**
> Job jobsPost(authorization, body)

Create a new job.

Creates a new Translation job. This job acts as a container for translation requests.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.JobApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

JobApi apiInstance = new JobApi();
String authorization = "authorization_example"; // String | Oauth2 token
CreateJob body = new CreateJob(); // CreateJob | Created job object
try {
    Job result = apiInstance.jobsPost(authorization, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#jobsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **body** | [**CreateJob**](CreateJob.md)| Created job object |

### Return type

[**Job**](Job.md)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

