
# UpdateJob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jobName** | **String** | The name of the translation job. |  [optional]
**description** | **String** | A description of the translation job. |  [optional]
**poReference** | **String** | PO Reference of the translation job. |  [optional]
**dueDate** | **String** | Due date of the translation job. Expected format is \&quot;yyyy-MM-dd\&quot; or with time expressed in GMT as: \&quot;yyyy-MM-ddTHH:mm:ssZ\&quot;  |  [optional]
**customData** | **String** | User specified custom data. |  [optional]
**shouldQuote** | **Boolean** | Request a quote for the job before translation proceeds.  TODO -  describe how quoting happens outside of REST API.  |  [optional]



