
# Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jobId** | **String** | Unique identifier representing a translation job. |  [optional]
**jobName** | **String** | The name of the translation job. | 
**description** | **String** | A description of the translation job. |  [optional]
**statusCode** | [**StatusCode**](StatusCode.md) |  |  [optional]
**hasError** | **Boolean** | If the translation request currently has an error. |  [optional]
**latestErrorMessage** | **String** | The most recent error message affecting the translation request. |  [optional]
**submitterId** | **String** | The ID of the user who is submitting the translation job. |  [optional]
**creatorId** | **String** | The ID of the user who is creating the translation job. |  [optional]
**providerId** | **String** | The ID of the provider that the translation job will be submitted to. |  [optional]
**poReference** | **String** | Purchase Order (PO) Reference of the translation job. |  [optional]
**dueDate** | [**OffsetDateTime**](OffsetDateTime.md) | Due date of the translation job. |  [optional]
**createdDate** | [**OffsetDateTime**](OffsetDateTime.md) | Date-time signature of when the translation job was created. |  [optional]
**modifiedDate** | [**OffsetDateTime**](OffsetDateTime.md) | Date-time signature of when the translation job was last modified. |  [optional]
**archived** | **Boolean** | If the translation job has been archived. |  [optional]
**jobStats** | [**JobStats**](JobStats.md) |  |  [optional]
**customData** | **String** | User specified custom data. |  [optional]
**shouldQuote** | **Boolean** | Request a quote for the job before translation proceeds.  TODO -  describe how quoting happens outside of REST API.  |  [optional]



