
# KeyValuePair

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | Name of the field. |  [optional]
**value** | **String** | Value for the field. |  [optional]



