# TranslationMemoryApi

All URIs are relative to *https://content-api.lionbridge.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsJobIdTmUpdatefilePut**](TranslationMemoryApi.md#jobsJobIdTmUpdatefilePut) | **PUT** /jobs/{jobId}/tm/updatefile | Add a file to existing translation memory of translation job.


<a name="jobsJobIdTmUpdatefilePut"></a>
# **jobsJobIdTmUpdatefilePut**
> jobsJobIdTmUpdatefilePut(authorization, jobId, body)

Add a file to existing translation memory of translation job.

Associates a file as the source content to existing translation memory of a job.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TranslationMemoryApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure OAuth2 access token for authorization: basicsecurity
OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

TranslationMemoryApi apiInstance = new TranslationMemoryApi();
String authorization = "authorization_example"; // String | Oauth2 token
String jobId = "jobId_example"; // String | Job ID.
CreateRequestUpdateTM body = new CreateRequestUpdateTM(); // CreateRequestUpdateTM | Request UpdateTM object
try {
    apiInstance.jobsJobIdTmUpdatefilePut(authorization, jobId, body);
} catch (ApiException e) {
    System.err.println("Exception when calling TranslationMemoryApi#jobsJobIdTmUpdatefilePut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Oauth2 token |
 **jobId** | **String**| Job ID. |
 **body** | [**CreateRequestUpdateTM**](CreateRequestUpdateTM.md)| Request UpdateTM object |

### Return type

null (empty response body)

### Authorization

[basicsecurity](../README.md#basicsecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

