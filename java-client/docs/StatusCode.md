
# StatusCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusCode** | [**StatusCodeEnum**](StatusCodeEnum.md) |  |  [optional]



