
# Token

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | This access token can be used to authorize requests. | 
**tokenType** | **String** | The token type will always be \&quot;bearer\&quot;. | 
**expiresIn** | **Integer** | The time in seconds the token will expire. |  [optional]
**refreshToken** | **String** | This token can be used to get a new access token. |  [optional]



