
# SupportAsset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supportassetId** | **String** | Unique identifier representing support asset. | 
**fileId** | **String** | Unique identifier representing asset file. | 
**jobId** | **String** | Unique identifier representing a translation job. | 
**filename** | **String** | Name of file | 
**filetype** | **String** | Type of file. | 



