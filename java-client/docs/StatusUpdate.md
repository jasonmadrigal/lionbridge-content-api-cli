
# StatusUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updateId** | **String** | Unique identifier representing a status update. |  [optional]
**jobId** | **String** | Unique identifier representing a translation job. |  [optional]
**requestIds** | **List&lt;String&gt;** |  |  [optional]
**acknowledged** | **Boolean** | Boolean if update has been acknowledged |  [optional]
**statusCode** | [**StatusCode**](StatusCode.md) |  |  [optional]
**updateTime** | [**OffsetDateTime**](OffsetDateTime.md) | Date-Time of the status update. |  [optional]
**hasError** | **Boolean** | If error has occurred with this status update. |  [optional]
**errorMessage** | **String** | Error message. |  [optional]



