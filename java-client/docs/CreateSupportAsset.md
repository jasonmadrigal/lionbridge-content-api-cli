
# CreateSupportAsset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileId** | **String** | ID of source file to use as this support asset. | 



