/*
 * The Lionbridge Content API
 * Enable translations with the Lionbridge Content API.
 *
 * OpenAPI spec version: 1.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.StatusCode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;

/**
 * StatusUpdate
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-06-13T15:53:37.388Z")
public class StatusUpdate {
  @SerializedName("updateId")
  private String updateId = null;

  @SerializedName("jobId")
  private String jobId = null;

  @SerializedName("requestIds")
  private List<String> requestIds = null;

  @SerializedName("acknowledged")
  private Boolean acknowledged = null;

  @SerializedName("statusCode")
  private StatusCode statusCode = null;

  @SerializedName("updateTime")
  private OffsetDateTime updateTime = null;

  @SerializedName("hasError")
  private Boolean hasError = null;

  @SerializedName("errorMessage")
  private String errorMessage = null;

  public StatusUpdate updateId(String updateId) {
    this.updateId = updateId;
    return this;
  }

   /**
   * Unique identifier representing a status update.
   * @return updateId
  **/
  @ApiModelProperty(value = "Unique identifier representing a status update.")
  public String getUpdateId() {
    return updateId;
  }

  public void setUpdateId(String updateId) {
    this.updateId = updateId;
  }

  public StatusUpdate jobId(String jobId) {
    this.jobId = jobId;
    return this;
  }

   /**
   * Unique identifier representing a translation job.
   * @return jobId
  **/
  @ApiModelProperty(value = "Unique identifier representing a translation job.")
  public String getJobId() {
    return jobId;
  }

  public void setJobId(String jobId) {
    this.jobId = jobId;
  }

  public StatusUpdate requestIds(List<String> requestIds) {
    this.requestIds = requestIds;
    return this;
  }

  public StatusUpdate addRequestIdsItem(String requestIdsItem) {
    if (this.requestIds == null) {
      this.requestIds = new ArrayList<String>();
    }
    this.requestIds.add(requestIdsItem);
    return this;
  }

   /**
   * Get requestIds
   * @return requestIds
  **/
  @ApiModelProperty(value = "")
  public List<String> getRequestIds() {
    return requestIds;
  }

  public void setRequestIds(List<String> requestIds) {
    this.requestIds = requestIds;
  }

  public StatusUpdate acknowledged(Boolean acknowledged) {
    this.acknowledged = acknowledged;
    return this;
  }

   /**
   * Boolean if update has been acknowledged
   * @return acknowledged
  **/
  @ApiModelProperty(value = "Boolean if update has been acknowledged")
  public Boolean isAcknowledged() {
    return acknowledged;
  }

  public void setAcknowledged(Boolean acknowledged) {
    this.acknowledged = acknowledged;
  }

  public StatusUpdate statusCode(StatusCode statusCode) {
    this.statusCode = statusCode;
    return this;
  }

   /**
   * Get statusCode
   * @return statusCode
  **/
  @ApiModelProperty(value = "")
  public StatusCode getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(StatusCode statusCode) {
    this.statusCode = statusCode;
  }

  public StatusUpdate updateTime(OffsetDateTime updateTime) {
    this.updateTime = updateTime;
    return this;
  }

   /**
   * Date-Time of the status update.
   * @return updateTime
  **/
  @ApiModelProperty(value = "Date-Time of the status update.")
  public OffsetDateTime getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(OffsetDateTime updateTime) {
    this.updateTime = updateTime;
  }

  public StatusUpdate hasError(Boolean hasError) {
    this.hasError = hasError;
    return this;
  }

   /**
   * If error has occurred with this status update.
   * @return hasError
  **/
  @ApiModelProperty(value = "If error has occurred with this status update.")
  public Boolean isHasError() {
    return hasError;
  }

  public void setHasError(Boolean hasError) {
    this.hasError = hasError;
  }

  public StatusUpdate errorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
    return this;
  }

   /**
   * Error message.
   * @return errorMessage
  **/
  @ApiModelProperty(value = "Error message.")
  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StatusUpdate statusUpdate = (StatusUpdate) o;
    return Objects.equals(this.updateId, statusUpdate.updateId) &&
        Objects.equals(this.jobId, statusUpdate.jobId) &&
        Objects.equals(this.requestIds, statusUpdate.requestIds) &&
        Objects.equals(this.acknowledged, statusUpdate.acknowledged) &&
        Objects.equals(this.statusCode, statusUpdate.statusCode) &&
        Objects.equals(this.updateTime, statusUpdate.updateTime) &&
        Objects.equals(this.hasError, statusUpdate.hasError) &&
        Objects.equals(this.errorMessage, statusUpdate.errorMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(updateId, jobId, requestIds, acknowledged, statusCode, updateTime, hasError, errorMessage);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StatusUpdate {\n");
    
    sb.append("    updateId: ").append(toIndentedString(updateId)).append("\n");
    sb.append("    jobId: ").append(toIndentedString(jobId)).append("\n");
    sb.append("    requestIds: ").append(toIndentedString(requestIds)).append("\n");
    sb.append("    acknowledged: ").append(toIndentedString(acknowledged)).append("\n");
    sb.append("    statusCode: ").append(toIndentedString(statusCode)).append("\n");
    sb.append("    updateTime: ").append(toIndentedString(updateTime)).append("\n");
    sb.append("    hasError: ").append(toIndentedString(hasError)).append("\n");
    sb.append("    errorMessage: ").append(toIndentedString(errorMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

