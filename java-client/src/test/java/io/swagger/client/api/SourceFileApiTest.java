/*
 * The Lionbridge Content API
 * Enable translations with the Lionbridge Content API.
 *
 * OpenAPI spec version: 1.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.Error;
import java.io.File;
import io.swagger.client.model.SourceFile;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for SourceFileApi
 */
@Ignore
public class SourceFileApiTest {

    private final SourceFileApi api = new SourceFileApi();

    
    /**
     * Upload a binary file to a job.
     *
     * Adds a file to the specified jobId. This can then be used to create a translation request. Or it can be used as a support asset to a job that has not been submitted.  *Note:* The legacy endpoint is still supported: https://ctt-rest-api-servlet-us-east-1.clay-tablet.net 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void jobsJobIdUploadPostTest() throws ApiException {
        String authorization = null;
        String jobId = null;
        String fileName = null;
        String fileType = null;
        File sourceFile = null;
        SourceFile response = api.jobsJobIdUploadPost(authorization, jobId, fileName, fileType, sourceFile);

        // TODO: test validations
    }
    
}
