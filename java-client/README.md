# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.FileApi;

import java.io.File;
import java.util.*;

public class FileApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure OAuth2 access token for authorization: basicsecurity
        OAuth basicsecurity = (OAuth) defaultClient.getAuthentication("basicsecurity");
        basicsecurity.setAccessToken("YOUR ACCESS TOKEN");

        FileApi apiInstance = new FileApi();
        String authorization = "authorization_example"; // String | Oauth2 token
        String jobId = "jobId_example"; // String | The ID of the job.
        String requestId = "requestId_example"; // String | The ID of the translation request.
        try {
            File result = apiInstance.jobsJobIdRequestsRequestIdRetrievefileGet(authorization, jobId, requestId);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling FileApi#jobsJobIdRequestsRequestIdRetrievefileGet");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://content-api.lionbridge.com/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*FileApi* | [**jobsJobIdRequestsRequestIdRetrievefileGet**](docs/FileApi.md#jobsJobIdRequestsRequestIdRetrievefileGet) | **GET** /jobs/{jobId}/requests/{requestId}/retrievefile | Retrieve the target content for translation request as a file.
*JobApi* | [**jobsGet**](docs/JobApi.md#jobsGet) | **GET** /jobs | Query all jobs.
*JobApi* | [**jobsJobIdArchivePut**](docs/JobApi.md#jobsJobIdArchivePut) | **PUT** /jobs/{jobId}/archive | Archive a job.
*JobApi* | [**jobsJobIdDelete**](docs/JobApi.md#jobsJobIdDelete) | **DELETE** /jobs/{jobId} | Delete a job.
*JobApi* | [**jobsJobIdGet**](docs/JobApi.md#jobsJobIdGet) | **GET** /jobs/{jobId} | Query job information.
*JobApi* | [**jobsJobIdPut**](docs/JobApi.md#jobsJobIdPut) | **PUT** /jobs/{jobId} | Update a job&#39;s information.
*JobApi* | [**jobsJobIdSubmitPut**](docs/JobApi.md#jobsJobIdSubmitPut) | **PUT** /jobs/{jobId}/submit | Send/Submit job to provider.
*JobApi* | [**jobsJobIdUnarchivePut**](docs/JobApi.md#jobsJobIdUnarchivePut) | **PUT** /jobs/{jobId}/unarchive | Unarchive a job.
*JobApi* | [**jobsPost**](docs/JobApi.md#jobsPost) | **POST** /jobs | Create a new job.
*ProviderApi* | [**providersGet**](docs/ProviderApi.md#providersGet) | **GET** /providers | Get all configured translation providers.
*ProviderApi* | [**providersProviderIdGet**](docs/ProviderApi.md#providersProviderIdGet) | **GET** /providers/{providerId} | Get a translation provider.
*RequestApi* | [**jobsJobIdRequestsAddPost**](docs/RequestApi.md#jobsJobIdRequestsAddPost) | **POST** /jobs/{jobId}/requests/add | Create new translation requests in a job.
*RequestApi* | [**jobsJobIdRequestsAddfilePost**](docs/RequestApi.md#jobsJobIdRequestsAddfilePost) | **POST** /jobs/{jobId}/requests/addfile | Add translation requests to a job based on a file.
*RequestApi* | [**jobsJobIdRequestsApprovePut**](docs/RequestApi.md#jobsJobIdRequestsApprovePut) | **PUT** /jobs/{jobId}/requests/approve | Approve translated content of specified request(s).
*RequestApi* | [**jobsJobIdRequestsGet**](docs/RequestApi.md#jobsJobIdRequestsGet) | **GET** /jobs/{jobId}/requests | Get all translation requests in a job.
*RequestApi* | [**jobsJobIdRequestsPut**](docs/RequestApi.md#jobsJobIdRequestsPut) | **PUT** /jobs/{jobId}/requests | Get specified translation requests in a job.
*RequestApi* | [**jobsJobIdRequestsRejectPut**](docs/RequestApi.md#jobsJobIdRequestsRejectPut) | **PUT** /jobs/{jobId}/requests/reject | Reject translated content of specified request(s).
*RequestApi* | [**jobsJobIdRequestsRequestIdDelete**](docs/RequestApi.md#jobsJobIdRequestsRequestIdDelete) | **DELETE** /jobs/{jobId}/requests/{requestId} | Delete the translation request in a job identified by requestId
*RequestApi* | [**jobsJobIdRequestsRequestIdGet**](docs/RequestApi.md#jobsJobIdRequestsRequestIdGet) | **GET** /jobs/{jobId}/requests/{requestId} | Get the translation request in a job identified by requestId
*RequestApi* | [**jobsJobIdRequestsUpdatecontentPut**](docs/RequestApi.md#jobsJobIdRequestsUpdatecontentPut) | **PUT** /jobs/{jobId}/requests/updatecontent | Add content to existing translation requests.
*RequestApi* | [**jobsJobIdRequestsUpdatefilecontentPut**](docs/RequestApi.md#jobsJobIdRequestsUpdatefilecontentPut) | **PUT** /jobs/{jobId}/requests/updatefilecontent | Add a file to existing translation requests.
*SourceFileApi* | [**jobsJobIdUploadPost**](docs/SourceFileApi.md#jobsJobIdUploadPost) | **POST** /jobs/{jobId}/upload | Upload a binary file to a job.
*StatusUpdateApi* | [**statusupdatesGet**](docs/StatusUpdateApi.md#statusupdatesGet) | **GET** /statusupdates | Get all unacknowledged status updates.
*StatusUpdateApi* | [**statusupdatesListenersGet**](docs/StatusUpdateApi.md#statusupdatesListenersGet) | **GET** /statusupdates/listeners | Query all Listeners.
*StatusUpdateApi* | [**statusupdatesListenersListenerIdDelete**](docs/StatusUpdateApi.md#statusupdatesListenersListenerIdDelete) | **DELETE** /statusupdates/listeners/{listenerId} | Delete a Listener.
*StatusUpdateApi* | [**statusupdatesListenersListenerIdGet**](docs/StatusUpdateApi.md#statusupdatesListenersListenerIdGet) | **GET** /statusupdates/listeners/{listenerId} | Query listener information.
*StatusUpdateApi* | [**statusupdatesListenersPost**](docs/StatusUpdateApi.md#statusupdatesListenersPost) | **POST** /statusupdates/listeners | Create a new Listener.
*StatusUpdateApi* | [**statusupdatesUpdateIdAcknowledgePut**](docs/StatusUpdateApi.md#statusupdatesUpdateIdAcknowledgePut) | **PUT** /statusupdates/{updateId}/acknowledge | Acknowledge a status update.
*SupportAssetApi* | [**jobsJobIdSupportassetsGet**](docs/SupportAssetApi.md#jobsJobIdSupportassetsGet) | **GET** /jobs/{jobId}/supportassets | Get all support assets in a job.
*SupportAssetApi* | [**jobsJobIdSupportassetsPost**](docs/SupportAssetApi.md#jobsJobIdSupportassetsPost) | **POST** /jobs/{jobId}/supportassets | Add support asset to a job.
*SupportAssetApi* | [**jobsJobIdSupportassetsSupportassetIdDelete**](docs/SupportAssetApi.md#jobsJobIdSupportassetsSupportassetIdDelete) | **DELETE** /jobs/{jobId}/supportassets/{supportassetId} | Delete the support asset in a job identified by supportassetId
*SupportAssetApi* | [**jobsJobIdSupportassetsSupportassetIdGet**](docs/SupportAssetApi.md#jobsJobIdSupportassetsSupportassetIdGet) | **GET** /jobs/{jobId}/supportassets/{supportassetId} | Get the support asset in a job identified by supportassetId
*TokenApi* | [**oauth2TokenGet**](docs/TokenApi.md#oauth2TokenGet) | **GET** /oauth2/token | Get a new token from Zendesk authentication.
*TokenApi* | [**oauth2TokenPost**](docs/TokenApi.md#oauth2TokenPost) | **POST** /oauth2/token | Get a new token.
*TranslationContentApi* | [**jobsJobIdRequestsRequestIdRetrieveGet**](docs/TranslationContentApi.md#jobsJobIdRequestsRequestIdRetrieveGet) | **GET** /jobs/{jobId}/requests/{requestId}/retrieve | Retrieve the target content for translation request(s).
*TranslationMemoryApi* | [**jobsJobIdTmUpdatefilePut**](docs/TranslationMemoryApi.md#jobsJobIdTmUpdatefilePut) | **PUT** /jobs/{jobId}/tm/updatefile | Add a file to existing translation memory of translation job.


## Documentation for Models

 - [ArrayOfRequestIds](docs/ArrayOfRequestIds.md)
 - [ArrayOfRequestIdsNote](docs/ArrayOfRequestIdsNote.md)
 - [CreateJob](docs/CreateJob.md)
 - [CreateListener](docs/CreateListener.md)
 - [CreateRequestFile](docs/CreateRequestFile.md)
 - [CreateRequestKeyValue](docs/CreateRequestKeyValue.md)
 - [CreateRequestUpdateTM](docs/CreateRequestUpdateTM.md)
 - [CreateSupportAsset](docs/CreateSupportAsset.md)
 - [CreateToken](docs/CreateToken.md)
 - [Error](docs/Error.md)
 - [Job](docs/Job.md)
 - [JobStats](docs/JobStats.md)
 - [KeyValuePair](docs/KeyValuePair.md)
 - [Listener](docs/Listener.md)
 - [ListenerAuthEnum](docs/ListenerAuthEnum.md)
 - [ListenerRequest](docs/ListenerRequest.md)
 - [ListenerTypeEnum](docs/ListenerTypeEnum.md)
 - [Provider](docs/Provider.md)
 - [ProviderId](docs/ProviderId.md)
 - [Request](docs/Request.md)
 - [SourceFile](docs/SourceFile.md)
 - [StatusCode](docs/StatusCode.md)
 - [StatusCodeEnum](docs/StatusCodeEnum.md)
 - [StatusUpdate](docs/StatusUpdate.md)
 - [SupportAsset](docs/SupportAsset.md)
 - [Token](docs/Token.md)
 - [TranslationContent](docs/TranslationContent.md)
 - [UpdateJob](docs/UpdateJob.md)
 - [UpdateRequestsFile](docs/UpdateRequestsFile.md)
 - [UpdateRequestsKeyValue](docs/UpdateRequestsKeyValue.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### basicsecurity

- **Type**: OAuth
- **Flow**: implicit
- **Authorization URL**: https://claytablet.zendesk.com/oauth/authorizations/new?response_type&#x3D;code&amp;client_id&#x3D;clay_tablet_rest_api&amp;scope&#x3D;read
- **Scopes**: 
  - all: do everything


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



