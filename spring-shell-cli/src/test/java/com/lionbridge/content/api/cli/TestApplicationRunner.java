package com.lionbridge.content.api.cli;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration
public class TestApplicationRunner implements ApplicationRunner {

    public TestApplicationRunner() {
        // Test app runner started.
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // Do nothing
    }
}
