package com.lionbridge.content.api.cli.service.impl;

import com.lionbridge.content.api.cli.service.ApiAccess;
import io.swagger.client.ApiException;
import io.swagger.client.api.TokenApi;
import io.swagger.client.model.CreateToken;
import io.swagger.client.model.Token;
import org.springframework.stereotype.Service;

@Service
public class DefaultApiAccess implements ApiAccess {

    @Override
    public Token getToken(String username, String password) throws ApiException {
        TokenApi tokenApi = new TokenApi();
        CreateToken createTokenBody = new CreateToken().username(username).password(password);
        Token token = tokenApi.oauth2TokenPost(createTokenBody);
        return token;
    }
}
