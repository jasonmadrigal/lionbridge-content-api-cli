package com.lionbridge.content.api.cli.command;

import com.lionbridge.content.api.cli.service.ApiAccess;
import io.swagger.client.ApiException;
import io.swagger.client.api.TokenApi;
import io.swagger.client.model.CreateToken;
import io.swagger.client.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.jline.FileInputProvider
import org.springframework.shell.standard.commands.Clear;
import org.springframework.util.StringUtils;

import java.io.Console;
import java.io.IOException;
import java.util.Scanner;

@ShellComponent
public class SimpleCli {

    private ApiAccess apiAccess;

    @Autowired
    public SimpleCli(ApiAccess apiAccess){
        this.apiAccess = apiAccess;
    }

    @ShellMethod(value = "Retrieves the authorization token", key = {"create token"})
    public String createToken( String username, String password) {
        Token result = null;
        try {
            TokenApi apiInstance = new TokenApi();
            CreateToken body = new CreateToken();
            body.setUsername(username);
            body.setPassword(password);
            result = apiInstance.oauth2TokenPost(body);
            return result.getAccessToken();
        } catch (ApiException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @ShellMethod(value = "Retrieves the access token to access the api", key = "login")
    public String login() throws IOException {

        Console console = System.console();
        String userName = console.readLine("Username: ");
        System.out.flush(); //println("");
        String password = new String(console.readPassword("Password: "));


//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Username: ");
//        String userName = scanner.nextLine();
//        System.out.print("Password: ");
//        String password = scanner.nextLine();


        String message = "Username: "+userName+" | Password: " + password;
//        System.out.println(message);
        return message;
    }
}
