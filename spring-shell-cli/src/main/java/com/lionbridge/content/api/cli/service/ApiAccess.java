package com.lionbridge.content.api.cli.service;

import io.swagger.client.ApiException;
import io.swagger.client.model.Token;

public interface ApiAccess {

    Token getToken(String username, String password) throws ApiException;

}
